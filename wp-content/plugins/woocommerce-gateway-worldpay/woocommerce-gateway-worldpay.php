<?php
/*
Plugin Name: WooCommerce WorldPay
Plugin URI: http://www.bigfatfrog.co.uk
Description: WorldPay Payment Gateway for WooCommerce. 
Version: 1.0.0
Author: _KDC-Labs
Author URI: http://www.www.bigfatfrog.co.uk
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Contributors: paul Riley
*/

add_action('plugins_loaded', 'woocommerce_gateway_worldpay_init', 0);
define('worldpay_IMG', WP_PLUGIN_URL . "/" . plugin_basename(dirname(__FILE__)) . '/assets/img/');

require_once('worldpay-callback.php');

function woocommerce_gateway_worldpay_init() {
	if ( !class_exists( 'WC_Payment_Gateway' ) ) return;

        /**
 	 * Gateway class
 	 */
	class WC_Gateway_WorldPay extends WC_Payment_Gateway {

	     /**
         * Make __construct()
         **/	
		public function __construct(){
			
			$this->id 	                = 'worldpay'; // ID for WC to associate the gateway values
			$this->method_title 	        = 'WorldPay'; // Gateway Title as seen in Admin Dashboad
			$this->method_description	= 'Payments via Worldpay'; // Gateway Description as seen in Admin Dashboad
			$this->has_fields 			= false; // Inform WC if any fileds have to be displayed to the visitor in Frontend 
			
			$this->init_form_fields();	// defines your settings to WC
			$this->init_settings();		// loads the Gateway settings into variables for WC

			$this->title 			= $this->settings['title']; // Title as displayed on Frontend
			$this->description 		= $this->settings['description']; // Description as displayed on Frontend
			if ( $this->settings['show_logo'] != "no" ) { // Check if Show-Logo has been allowed
			     $this->icon 			= worldpay_IMG . $this->settings['show_logo'] ;
			}
                        $this->key_id 			        = $this->settings['key_id'];
                        $this->key_secret 		        = array_key_exists('key_secret',$this->settings) ? $this->settings['key_secret'] :null;
                        $this->username 			= array_key_exists('username',$this->settings) ? $this->settings['username'] : null;
                        $this->password 			= $this->settings['password'];
//			if ( $this->settings['payment_mode'] == "manual" ) { // Check if Set to Mannual to generate chmod value.
//				$payment_mode = array();
//				$payment_mode[] .= ($this->settings['payment_mode_pg']=='yes')?'pg':'';
//				$payment_mode[] .= ($this->settings['payment_mode_nb']=='yes')?'nb':'';
//				$payment_mode[] .= ($this->settings['payment_mode_ppc']=='yes')?'ppc':'';
//				$this->payment_mode 	= implode( '_', array_filter( $payment_mode ) );
//			}
//                       $this->redirect_page 	= $this->settings['redirect_page'];

                        $this->msg['message']	= '';
                        $this->msg['class'] 	= '';
			
			add_action('init', array(&$this, 'check_worldpay_response'));
                      //  add_action('woocommerce_api_' . strtolower(get_class($this)), array($this, 'check_worldpay_response')); //update for woocommerce >2.0

                        if ( version_compare(WOOCOMMERCE_VERSION, '2.0.0', '>=' ) ) {
                                add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) ); //update for woocommerce >2.0
                             } else {
                                add_action( 'woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) ); // WC-1.6.6
                            }
                        add_action('woocommerce_receipt_worldpay', array(&$this, 'receipt_page'));	
		} //END-__construct
		
        /**
         * Initiate Form Fields in the Admin Backend
         **/
		function init_form_fields(){

			$this->form_fields = array(
				// Activate the Gateway
				'enabled' => array(
					'title'         => __('Enable/Disable:', 'woo_worldpay'),
					'type'          => 'checkbox',
					'label' 	=> __('Enable WorldPay', 'woo_worldpay'),
					'default' 	=> 'no',
					'description' 	=> 'Show in the Payment List as a payment option'
				),
				// Title as displayed on Frontend
      			
                                        'title' => array(
					'title' 			=> __('Title:', 'woo_worldpay'),
					'type'			=> 'text',
					'default' 		=> __('Online Payments', 'woo_worldpay'),
					'description' 	=> __('This controls the title which the user sees during checkout.', 'woo_worldpay'),
					'desc_tip' 		=> true
				),
				// Description as displayed on Frontend
      			                'description' => array(
					'title' 			=> __('Description:', 'woo_worldpay'),
					'type' 			=> 'textarea',
					'default' 		=> __('Pay securely by Credit or Debit card or internet banking through WorldPay.', 'woo_worldpay'),
					'description' 	=> __('This controls the description which the user sees during checkout.', 'woo_worldpay'),
					'desc_tip' 		=> true
				),
				// Key-ID
      			                'key_id' => array(
					'title' 			=> __('Merchant Id:', 'woo_worldpay'),
					'type' 			=> 'text',
					'description' 	=> __('Available from WorldPay'),
					'desc_tip' 		=> false
				),

                            	// Key-ID
      			                'password' => array(
					'title' 			=> __('Password:', 'woo_worldpay'),
					'type' 			=> 'text',
					'description' 	=> __('Worlpay redirect password'),
					'desc_tip' 		=> false
				),

  				// Show Logo on Frontend
      			        'show_logo'             => array(
					'title' 		=> __('Show Logo:', 'woo_worldpay'),
					'type' 			=> 'select',
					'label' 		=> __('Logo on Checkout Page', 'woo_worldpay'),
					'options' 		=> array('no'=>'No Logo','poweredByWorldPay.gif'=>'WorldPay'),
					'default' 		=> 'no',
					'description'           => __('<strong>Logos</strong>: <img src="'. worldpay_IMG . 'poweredByWorldPay.gif" height="24px" /> '),
                                      	'desc_tip' 		=> false
                                        ),
  				// Live or Test
      			         'test'             => array(
					'title' 		=> __('Live or test:', 'woo_worldpay'),
					'type' 			=> 'select',
					'label' 		=> __('Process payments on WorldPay Live or Test system', 'woo_worldpay'),
					'options' 		=> array(true=>'Test',false=>'** Live **'),
					'default' 		=> true,
					'desc_tip' 		=> false
                                        )
			);

		} //END-init_form_fields
		
        /**
         * Admin Panel Options
         * - Show info on Admin Backend
         **/
		public function admin_options(){
			global $woocommerce;

			// Redirect URL
			if ( $this->redirect_page == '' || $this->redirect_page == 0 ) {
				$return_url = get_permalink( get_option ( 'woocommerce_myaccount_page_id' ) );
			} else {
				$redirect_url_static = get_permalink( $this->redirect_page );
			}
			echo '<h3>'.__('WorldPay', 'woo_worldpay').'</h3>';
			echo '<p>'.__('process card payments via WorldPay', 'woo_worldpay').'</p>';
			
			echo '<table class="form-table">';
			// Generate the HTML For the settings form.
			$this->generate_settings_html();
			echo '</table>';
		} //END-admin_options

        /**
         *  There are no payment fields, but we want to show the description if set.
         **/
		function payment_fields(){
			if( $this->description ) {
				echo wpautop( wptexturize( $this->description ) );
			}
		} //END-payment_fields
		
        /**
         * Receipt Page
         **/
		function receipt_page($order){
			echo '<p><strong>' . __('Thank you for your order.', 'woo_worldpay').'</strong><br/>' . __('The payment page will open soon.', 'woo_worldpay').'</p>';
			echo $this->generate_worldpay_form($order);
		} //END-receipt_page
    
        /**
         * Generate button link
         **/
		function generate_worldpay_form($order_id){
			global $woocommerce;
			$order = new WC_Order( $order_id );

			// Transaction ID
			$order_number = $order_id.'-'.date("ymdhms");
			
			// Address Billing
			if($order->billing_address_2 != "") { // Check if Address Line 2 is set.
				$address = $order->billing_address_1.", ".$order->billing_address_2;
			} else {
				$address = $order->billing_address_1;
			}
                        if($order->shipping_address_2 != "") { // Check if Address Line 2 is set.
                                $delvAddress = $order->shipping_address_1.", ".$order->shipping_address_2;
			} else {
				$delvAddress = $order->shipping_address_1;
			}


            $payment_page = get_permalink( woocommerce_get_page_id( 'checkout' ) );
            $url = $payment_page . 'order-received';
			
			$worldpay_args = array(
                'fixContact'    => true,
				'email' 	=> $order->billing_email,
				'tel'       => is_null($order->billing_phone)?$order->shipping_phone:$order->billing_phone,
				
				'name'      => $order->billing_last_name,
				'address1' 	=> $order->billing_address_1,
                'address2' 	=> $order->billing_address_2, 
				'town' 		=> $order->billing_city,
			
				'country' 	=> $order->billing_country,
				'postcode' 	=> $order->billing_postcode,
				'amount' 	=> number_format($order->order_total, 2, '.', ''),
				'cartId' 		=> $order_number,
				//'privatekey' 	=> $privatekey,
				 'instId' 		=> $this->key_id,
				
				'hideCurrency' =>"true",
				'currency' 		=> 'GBP',
				'noLanguageMenu' => "true",
				 'language'              => 'en',
				
				'withDelivery'          =>"true",
                'delvTel'       => $order->shipping_phone,
				'delvName'      => $order->shipping_last_name,
				'delvAddress1' 	=> $order->shipping_address_1,
                'delvAddress2' 	=> $order->shipping_address_2,
				'delvTown' 		=> $order->shipping_city,
			
				'delvCountry' 	=> $order->shipping_country,
				'delvPostcode' 	=> $order->shipping_postcode,
                  'MC_callback' => $url
			
			);
			
			// remove any empty fields - causes a problem with WorlPay
			foreach($worldpay_args as $key => $value)
             if (empty($worldpay_args[$key])) {
				unset($worldpay_args[$key]);
			}
			
		
         
			//if this is test
			if ($this->settings['test']) {
				$worldpay_args['testMode'] = "100";
				$url = "https://secure-test.worldpay.com/wcc/purchase";
			}
			else {
				$worldpay_args['testMode'] = "0";
				$url = "https://secure.worldpay.com/wcc/purchase";
			}
                        

			$worldpay_args_array = array();
			foreach($worldpay_args as $key => $value){
				$worldpay_args_array[] = '                <input type="hidden" name="'.$key.'" value="'.$value.'">'."\n";
			}
			
			$form =  '	<form action="'. $url .'" method="post" id="worldpay_payment_form" >
  				' . implode('', $worldpay_args_array) . '
				<input type="submit" class="button-alt" id="submit_worldpay_payment_form" value="'.__('Pay via WorldPay', 'woo_worldpay').'" /> <a class="button cancel" href="'.$order->get_cancel_order_url().'">'.__('Cancel order &amp; restore cart', 'woo_worldpay').'</a>
					<script type="text/javascript">
					jQuery(function(){
					jQuery("body").block({
						message: "'.__('Thank you for your order. We are now redirecting you to Payment Gateway to make payment.', 'woo_worldpay').'",
						overlayCSS: {
							background		: "#fff",
							opacity			: 0.6
						},
						css: {
							padding			: 20,
							textAlign		: "center",
							color			: "#555",
							border			: "3px solid #aaa",
							backgroundColor	: "#fff",
							cursor			: "wait",
							lineHeight		: "32px"
						}
					});
					  jQuery("#submit_worldpay_payment_form").click();}); 
					</script> 
				</form>';		
				
				return $form;
		
		} //END-generate_worldpay_form

        /**
         * Process the payment and return the result
         **/
        function process_payment($order_id){
			global $woocommerce;
            $order = new WC_Order($order_id);

			if ( version_compare( WOOCOMMERCE_VERSION, '2.1.0', '>=' ) ) { // For WC 2.1.0
			  	$checkout_payment_url = $order->get_checkout_payment_url( true );
			} else {
				$checkout_payment_url = get_permalink( get_option ( 'woocommerce_pay_page_id' ) );
			}

			return array(
				'result' => 'success', 
				'redirect' => add_query_arg(
					'order', 
					$order->id, 
					add_query_arg(
						'key', 
						$order->order_key, 
						$checkout_payment_url						
					)
				)
			);
		} //END-process_payment

      

	} //END-class




	
	/**
 	* Add the Gateway to WooCommerce
 	**/
	function woocommerce_add_gateway_worldpay_gateway($methods) {
		$methods[] = 'WC_Gateway_WorldPay';
		return $methods;
	}//END-wc_add_gateway
	
	add_filter('woocommerce_payment_gateways', 'woocommerce_add_gateway_worldpay_gateway' );
	
} //END-init

/**
* 'Settings' link on plugin page
**/
add_filter( 'plugin_action_links', 'worldpay_add_action_plugin', 10, 5 );
function worldpay_add_action_plugin( $actions, $plugin_file ) {
	static $plugin;

	if (!isset($plugin))
		$plugin = plugin_basename(__FILE__);
	if ($plugin == $plugin_file) {

			$settings = array('settings' => '<a href="admin.php?page=wc-settings&tab=checkout&section=wc_gateway_worldpay">' . __('Settings') . '</a>');
		
    			$actions = array_merge($settings, $actions);
			
		}
		
		return $actions;
}//END-settings_add_action_link