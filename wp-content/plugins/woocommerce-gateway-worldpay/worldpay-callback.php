<?php
/* This script is called by WorldPay (set up on the admin screen) with a post request     */
/* Worlpday then displays the return value - so a redirect is output to redirect the page */
/* back to the shop                                                                       */
/* meta redirect needed as script tags are removed by WorldPay                            */


add_filter('query_vars', 'parameter_queryvars' );
function parameter_queryvars( $qvars )
{
    $qvars[] = 'action';
    return $qvars;
}

function worldpay_callback( $query ) {    
  
 
    if ( $query->is_main_query() ) {
      
        if ( array_key_exists('action', $query->query) && $query->query['action'] == 'worldpay' ) {

            $payment_page = get_permalink( woocommerce_get_page_id( 'checkout' ) );
            $url = $payment_page . 'order-received';
            
    
            $paymentResponePassword = 'c0ff33_0rd3r';
            $mail = true;
            $callbackPW = isset($_POST['callbackPW'])? $_POST['callbackPW']:'Not Set';

            $cartId = isset($_POST['cartId'])? explode('-',$_POST['cartId']):array(0=>null);

			$orderId = $cartId[0];
		

            /* Check the callback password */
            if ($callbackPW !== $paymentResponePassword) {

                echo 'There has been a communication error between WorldPay and Tilley Green. <br/>' .
                     'We are aware of the problem but please contact TilleyGreen for assistance.';
                     if ($mail) {
                         mail('paulriley@grocontinental.co.uk','TilleyGreen Callback Password Error',  $callbackPW );
                     }
                return;

            }
            else
            {

              
               if ($_POST['rawAuthCode'] == 'A') {   
                    $auth = 'Completed';
                }
                elseif ($_POST['rawAuthCode'] == 'C')
                {
                    $auth = 'Cancelled';     
                }
                elseif ($_POST['rawAuthCode'] == 'D')
                { //do nothing await for customer to get in contact}
                     $auth = 'On-hold'; 
                }

                /* ************************************************
                 *  Update database
                   *************************************************/

                
                $order = new WC_Order($orderId);  
                if (!empty($order)) {

                    $order->update_status( $auth ,' Worldpay callback');
                }

                $url .= '?auth=' .$auth .'&cartId=' . $orderId ;

                //do the redirect
                 echo  '<meta http-equiv="refresh" content="0;URL='  .$url .  '" />';   
                 die();

            }

        }
    } // end of main query
}
add_action( 'pre_get_posts', 'worldpay_callback' );


?>