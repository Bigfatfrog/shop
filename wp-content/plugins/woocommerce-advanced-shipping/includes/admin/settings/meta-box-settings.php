<?php
/**
 * WAFS meta box settings.
 *
 * Display the shipping settings in the meta box.
 *
 * @author		Paul Riley
 * @package		WooCommerce Advanced Shipping
 * @version		1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

wp_nonce_field( 'wafs_settings_meta_box', 'wafs_settings_meta_box_nonce' );

global $post;
$settings = get_post_meta( $post->ID, '_wafs_shipping_method', true );
$settings['shipping_title'] = ! empty( $settings['shipping_title'] ) ? $settings['shipping_title'] : '';
$settings['shipping_rate'] = ! empty( $settings['shipping_rate'] ) ? $settings['shipping_rate'] : '';

?><div class='wafs wafs_settings wafs_meta_box wafs_settings_meta_box'>

	<p class='wafs-option'>

		<label for='shipping_title'><?php _e( 'Shipping title', 'woocommerce-advanced-shipping' ); ?></label>
		<input type='text' class='' id='shipping_title' name='_wafs_shipping_method[shipping_title]'
			value='<?php echo esc_attr( $settings['shipping_title'] ); ?>' placeholder='<?php _e( 'e.g. Free Shipping', 'woocommerce-advanced-shipping' ); ?>'>
<br/>
		<label for='shipping_rate'><?php _e( 'Shipping rate', 'woocommerce-advanced-shipping' ); ?></label>
		<input type='text' class='' id='shipping_rate' name='_wafs_shipping_method[shipping_rate]'
			value='<?php echo esc_attr( $settings['shipping_rate'] ); ?>' placeholder='<?php _e( 'e.g. 2.99 ', 'woocommerce-advanced-shipping' ); ?>'>
<br/>

		<label for='shipping_tax'><?php _e( 'Tax status', 'woocommerce-advanced-shipping' ); ?></label>
		<select id='shipping_tax' name='_wafs_shipping_method[shipping_tax]' >
                        <option value="0"><?php _e( 'None', 'Tax status', 'woocommerce-advanced-shipping' ) ?></option>
                        <option value="1" <?php echo ( $settings['shipping_tax']  == 1 ?' selected ' : '')?> ><?php _e( 'Taxable', 'woocommerce-advanced-shipping' ) ?></option>
                </select>
                
                
        </p>


</div>
