function ajaxformsendmail(name,email,phone,contents,keepintouch) {

    document.getElementById("ajaxcontactname").disabled = true;
    document.getElementById("ajaxcontactemail").disabled = true; 
    document.getElementById("ajaxcontactphone").disabled = true;
    document.getElementById("ajaxcontactcontents").disabled = true;
    document.getElementById("ajaxcontactkeepintouch").disabled = true;
    document.getElementById("ajax").style.display = 'block';
    

 
 
    jQuery.ajax({

        type: 'POST',
        url: ajaxcontactajax.ajaxurl,
        data: {
            action: 'ajaxcontact_send_mail',
            acfname: name,
            acfemail: email,
            acfphone:phone,
            acfcontents:contents,
            acfkeepintouch: keepintouch
            },
        success:function(data, textStatus, XMLHttpRequest){
            var id = '#ajaxcontact-response';
            jQuery(id).html('');
            jQuery(id).append(data);
          
            
            document.getElementById("woo-ajaxcontact-response").style.display = 'block';
            
            var el = document.getElementById('ajaxcontact-response');
            removeClass(el, 'woocommerce-error');
            addClass(el, 'woocommerce-message');
                
            if(data.length < 60){
                document.getElementById("ajaxcontactname").disabled = false;
                document.getElementById("ajaxcontactemail").disabled = false; 
                document.getElementById("ajaxcontactphone").disabled = false;
                document.getElementById("ajaxcontactcontents").disabled = false;
                document.getElementById("ajaxcontactkeepintouch").disabled = false;
                removeClass(el, 'woocommerce-message');
                addClass(el, 'woocommerce-error');
            }
                document.getElementById("ajax").style.display = 'none';
                
            },

        error: function(MLHttpRequest, textStatus, errorThrown){
            alert(errorThrown);

            }

        });

}


function hasClass(el, className) {
  if (el.classList)
    return el.classList.contains(className)
  else
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
    el.className=el.className.replace(reg, ' ')
  }
}


 $(document).ready(function() {
        var carouselContainer = $('.carousel');
        var slideInterval = 3000;

        function toggleCaption() {
            $('.carousel-caption').hide();
            var caption = carouselContainer.find('.active').find('.carousel-caption');
            caption.delay(500).toggle("slide", {direction:'left'});
            caption.delay(4000).toggle("slide", {direction:'right'});
        }


        carouselContainer.carousel({
            interval: slideInterval,
            cycle: true,
            pause: "hover"
        }).on('slid.bs.carousel', function() {
            toggleCaption();
        });
        
         toggleCaption();
 })