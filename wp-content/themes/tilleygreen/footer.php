<!-- 
<div class="col-xs-12 card-images" >

        <img src="<?php bloginfo('template_url') ?>/assets/cardLogos/poweredByWorldPay.gif">
        <img src="<?php bloginfo('template_url') ?>/assets/cardLogos/VISA.gif">
        <img src="<?php bloginfo('template_url') ?>/assets/cardLogos/mastercard.gif">
        <img src="<?php bloginfo('template_url') ?>/assets/cardLogos/maestro.gif">
        <img src="<?php bloginfo('template_url') ?>/assets/cardLogos/amex-logo2.gif">
                <img src="<?php bloginfo('template_url') ?>/assets/cardLogos/JCB.gif">
   
</div>
-->

<div id="footer">

    <div class="contact col-xs-12 col-sm-4"><span>
        CONTACT<br/>
        <b>Tilley Green Coffee Company</b><br/>
        Shakespeare Way <br/>
        Whitchurch Business Park <br/>
        Whitchurch <br/>
        Shropshire <br/>
        SY13 1LJ <br/>
        <b>Telephone</b> 01948 841426 <br/>
        <b>Email</b> info@tilleygreencoffee.co.uk <br/>
        </span></div>
    <div class="col-xs-12 col-sm-4"><a class="tandc" href='<?php bloginfo('url')?>/?page_id=142'>TERMS & CONDITIONS</a></div>  
    <div class="col-xs-12 col-sm-4">
        <img src="<?php bloginfo('template_url')?>/assets/pictures/footerBg.jpg">
    </div>
    <div class="registered-office colx-xs-12">
        Registered in England and Wales number 08387889. Registered office at Riverbank House, Tibberton, Newport, Shropshire TF10 8NN.  
    </div>
</div> <!-- footer -->

</div> <!-- wrapper -->
<?php wp_footer(); ?>
</body>
</html>