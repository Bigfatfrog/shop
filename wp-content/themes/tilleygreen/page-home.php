<?php get_header(); ?>
<div id="main">


        <div id="myCarousel" class="carousel slide " data-ride="carousel">


          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">

                 <?php if($image = get_field('slider_image_1',10)) 
                    echo "<div class='item active'><img src='".$image['sizes']['slider']."' alt='".$image['alt']."'>"
                         . '<div class="carousel-caption LoveYouLikeASister"><span>' . $image['alt'] . '</span></div>'
                         . "</div>";?>
                 <?php if($image = get_field('slider_image_2',10)) 
                    echo "<div class='item '><img src='".$image['sizes']['slider']."' alt='".$image['alt']."'>"
                          . '<div class="carousel-caption LoveYouLikeASister"><span>' . $image['alt'] . '</span></div>'
                         . "</div>"; ?>
                 <?php if($image = get_field('slider_image_3',10)) 
                    echo "<div class='item '><img src='".$image['sizes']['slider']."' alt='".$image['alt']."'>"
                      . '<div class="carousel-caption LoveYouLikeASister"><span>' . $image['alt'] . '</span></div>'
                         . "</div>"; ?>
                 <?php if($image = get_field('slider_image_4',10)) 
                    echo "<div class='item '><img src='".$image['sizes']['slider']."' alt='".$image['alt']."'>"
                          . '<div class="carousel-caption LoveYouLikeASister"><span>' . $image['alt'] . '</span></div>'
                         . "</div>"; ?>
                 <?php if($image = get_field('slider_image_5',10)) 
                    echo "<div class='item '><img src='".$image['sizes']['slider']."' alt='".$image['alt']."'>"
                          . '<div class="carousel-caption LoveYouLikeASister"><span>' . $image['alt'] . '</span></div>'
                         . "</div>"; ?> 
                 <?php if($image = get_field('slider_image_6',10)) 
                    echo "<div class='item '><img src='".$image['sizes']['slider']."' alt='".$image['alt']."'>"
                          . '<div class="carousel-caption LoveYouLikeASister"><span>' . $image['alt'] . '</span></div>'
                         . "</div>"; ?>


          </div>
          
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
             <li data-target="#myCarousel" data-slide-to="3"></li>
              <li data-target="#myCarousel" data-slide-to="4"></li>
               <li data-target="#myCarousel" data-slide-to="5"></li>
        </ol>

        </div>
     <hr/>
    <div id="content">
    
   <?php  echo do_shortcode('[featured_products per_page="12" columns="4"]'); ?>

</div>

</div>
<div id="delimiter">
</div>
<?php get_footer(); ?>