<?php get_header(); ?>

<section id="primary" class="site-content">

<div id="content" role="main">

        <?php

        custom_breadcrumbs();
        if ( have_posts() ) : ?>
      

        <?php while ( have_posts() ) : the_post(); ?>

      
        <div class="col-xs-12 col-sm-4 cat-img-div" style="margin:auto">
        <?php
            if ( has_post_thumbnail( $_post->ID, 'thumbnail' ) ) {
                echo '<a href="' . get_permalink( $_post->ID ) . '" title="' . esc_attr( $_post->post_title ) . '">';
                echo get_the_post_thumbnail( $_post->ID, 'thumbnail',array('class' => 'img-responsive') );
                echo '</a>';
            } ?>
        </div>
    
        <div class="col-xs-12 col-sm-8">
            <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>         
            <small><?php the_time('F jS, Y') ?></small> <br/>

                <div class="entry">

                    <?php the_excerpt(); ?>

                </div>
        </div>    
    
        <hr style="width:80%;padding:20px;">
        
        <?php endwhile; // End Loop

        else: ?>

            <p>Sorry, no posts matched your criteria.</p>

        <?php endif; ?>

</div>

</section>

<?php get_footer(); ?>
