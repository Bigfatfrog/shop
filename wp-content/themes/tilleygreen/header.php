<!DOCTYPE html>
<html>
    
    
<head>
<title><?php echo get_bloginfo('name')?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php  wp_head(); ?>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<!-- eu cookie law -->
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('url')?>/../jquery-eu-cookie-law-popup-master/css/jquery-eu-cookie-law-popup.css">
<script src="<?php echo get_bloginfo('url')?>/../jquery-eu-cookie-law-popup-master/js/jquery-eu-cookie-law-popup.js"></script>
</head>
<body 
<?php body_class("eupopup eupopup-top"); ?> ontouchstart="">
<div id="wrapper">
<div id="header">
  
    
<nav class="navbar navbar-default">
  <div class="container-fluid">
      
      <?php
    wp_nav_menu( array( 'menu' => 'Top menu', 'container' => 'div', 
        'container_class' => 'top-menu', 'container_id' => '', 'menu_class' => 'menu', 'menu_id' => '',
    'echo' => true, 'fallback_cb' => 'wp_page_menu', 'before' => '', 'after' => '',  
        'link_before' => '<i class="fa fa-facebook-square fa-2x"></i>'
                       . '<i class="fa fa-twitter-square fa-2x"></i>'
                       . '<i class="fa fa-instagram fa-2x"></i>',
        'link_after' => '', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    'depth' => 0   ));
 
 ?> 
      
      
        <div class="navbar-header">
               <a class="navbar-brand" href="<?php bloginfo('url')?>"><img src="<?php bloginfo('template_url')?>/assets/pictures/logo.gif" alt="Tilley Green Logo"/></a>
               
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

     
        </div>
      <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <?php
            wp_nav_menu( array(
              'menu' => 'header-menu',
              'depth' => 2,
              'container' => false,
              'menu_class' => 'nav navbar-nav',
              //Process nav menu using our custom nav walker
              'walker' => new wp_bootstrap_navwalker())
            );
            ?>
      </div>
    </div>
</nav>

  

</div><!-- header -->