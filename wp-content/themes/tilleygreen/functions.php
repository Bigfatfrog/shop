<?php

add_action( 'after_setup_theme', 'tilley_theme_setup' );
function tilley_theme_setup() {
    add_image_size( 'slider', 921, 400, true ); // (cropped)
    add_image_size( 'thumbnail', 307, 200, true ); // (cropped)
    add_image_size( 'cup', 250, 250, true );
    add_image_size( 'front-page-product-image', 300,300, true);
}

/**
 * Changes the redirect URL for the Return To Shop button in the cart.
 *
 * @return string
 */
function wc_empty_cart_redirect_url() {
   $test = get_bloginfo('url');
	return get_bloginfo('url').'/index.php/product-category/coffee/';
}
add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );

function wc_continue_shopping_redirect( $return_to ) {
    $test = get_bloginfo('url');
	return get_bloginfo('url').'/index.php/product-category/coffee/';
}
add_filter( 'woocommerce_continue_shopping_redirect', 'wc_continue_shopping_redirect', 20 );



function register_tilley_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
       'top-menu' => __( 'Top Menu' )
    )
  );
}
add_action( 'init', 'register_tilley_menus' );

require_once('wp_bootstrap_navwalker.php');

// add taxoonomy term to body_class
function woo_custom_taxonomy_in_body_class( $classes ){

  if( is_singular( 'product' ) )
  {
    $custom_terms = get_the_terms(0, 'product_cat');
    if ($custom_terms) {
      foreach ($custom_terms as $custom_term) {
        $classes[] = 'product_cat_' . $custom_term->slug;
      }
    }
  }
  return $classes;
}
add_filter( 'body_class', 'woo_custom_taxonomy_in_body_class' );

add_action( 'woocommerce_thankyou', 'order_email_workaround' );

function order_email_workaround ($order_id) {
    global $woocommerce;
    $mailer = $woocommerce->mailer();
    // Email customer with order-processing receipt
    $email = $mailer->emails['WC_Email_Customer_Processing_Order'];
    $email->trigger( $order_id );
    // Email admin with new order email
    $email = $mailer->emails['WC_Email_New_Order'];
    $email->trigger( $order_id );
}

add_action( 'woocommerce_payment_complete', 'order_complete_email_workaround' );

// solve problems with woo commerce emails
// https://wordpress.org/support/topic/woocommercenew-order-not-send-emails
function order_complete_email_workaround ($order_id) {

    if(!empty($order_id)) {
        $order = new WC_Order($order_id);
        $order->update_status('completed');
    }
	
// commented out to void fatal error with paypal order return - don't think
// it's need on new server - PR 17/06/2017	
	
 //  global $woocommerce;
 //  $mailer = $woocommerce->mailer();
 //  $email = $mailer->emails['WC_Email_Customer_Completed_order'];
 //  $email->trigger( $order_id );
}

//function update_wc_order_status($posted) {
	
//    $order_id = isset($posted['invoice']) ? $posted['invoice'] : '';
//    if(!empty($order_id)) {
//        $order = new WC_Order($order_id);
//        $order->update_status('completed');
//    }
//}
//add_action('paypal_ipn_for_wordpress_payment_status_completed', 'update_wc_order_status', 10, 1);


function convert_child_links( $link){
    
    $regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@";
    
    preg_match($regex, $link, $matches);
    $newUrl = wp_get_post_parent_id(url_to_postid(rtrim($matches[0],'"')));

    if (isset($newUrl)){
         $link = preg_replace($regex, get_permalink($newUrl) . '"' , $link);
    }
    
   return $link;
    
}

add_filter ('woocommerce_cart_item_name','convert_child_links');

function wpa_change_my_basket_text( $translated_text, $text, $domain ){

    if($translated_text == 'Update Cart' ) $translated_text = 'Update Basket';
    if($translated_text == 'Cart Totals' ) $translated_text = 'Basket Totals';
    
    return $translated_text;
}
add_filter( 'gettext', 'wpa_change_my_basket_text', 10, 3 );

function get_thankyou_order_id () {
    
    return (isset($_GET['cartId'])?$_GET['cartId']:null);

}
add_filter('woocommerce_thankyou_order_id','get_thankyou_order_id') ;

function get_thankyou_order_key() {
    //woo has a order id and order key that must match on the thank you page
    // don't think we need that level of sophistication here - so just get the key from the id.
    
    $orderId = (isset($_GET['cartId'])?$_GET['cartId']:null);
    $orderKeyArray =  get_post_meta ( $orderId, '_order_key');
    return isset($orderKeyArray[0])? $orderKeyArray[0]:null;
 
}
add_filter('woocommerce_thankyou_order_key','get_thankyou_order_key') ;

//enqueue bootstrap and font awsome
function reg_scripts() {
   
    // try and stop an ie error
   // wp_enqueue_script( 'json2', get_template_directory_uri() . '/js/json2.js', array(), true );
    
  
    wp_enqueue_script( 'json2', get_template_directory_uri() . '/js/contact.js', array(), true );
     
    // Bootstrap
    wp_register_style('bootstrap-styles', '//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css', array(), null, 'all');
    wp_enqueue_style('bootstrap-styles');
    
    // Theme Styles
    wp_register_style('theme-styles', get_stylesheet_uri(), array(), null, 'all');
    wp_enqueue_style('theme-styles');
   
    // Font Awesome
    wp_register_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), null, 'all');
    wp_enqueue_style('font-awesome');

    // jQuery
    wp_deregister_script('jquery');
    wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', array(), null, false);
    wp_enqueue_script('jquery');
    // Bootstrap
    wp_register_script('bootstrap-scripts', '//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js', array('jquery'), null, true);
    wp_enqueue_script('bootstrap-scripts');
	
    
    
    
    
    
   
}
add_action('wp_enqueue_scripts', 'reg_scripts');
 



// Breadcrumbs
function custom_breadcrumbs() {
       
    // Settings
    $separator          = '|';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Home';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
      
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}

function createAjaxContactForm() {
	return '

<form id="ajaxcontactform" action="" method="post"enctype="multipart/form-data">

<div id="ajaxcontact-text">

<div class="woocommerce" id="woo-ajaxcontact-response" style="display:none">
<div class="woocommerce-message" id="ajaxcontact-response" ></div>
</div>
<label for="ajaxcontactname" >Name<abbr class="required" title="required">*</abbr></label> <input class="form-control" type="text" id="ajaxcontactname" name="ajaxcontactname" required/><br />

<br/>

<label for="ajaxcontactemail" >Email<abbr class="required" title="required">*</abbr> </label> <input class="form-control" type="email" id="ajaxcontactemail" name="ajaxcontactemail" required/><br />

<br/>

<label for="ajaxcontactphone" >Tel no </label> <input class="form-control" type="tel" id="ajaxcontactphone" name="ajaxcontactphone"/><br />

<br/>


<label for="ajaxcontactcontents">Message <abbr class="required" title="required">*</abbr></label> 

<textarea class="form-control" id="ajaxcontactcontents" name="ajaxcontactcontents"  ></textarea><br/>
<div id="keep-in-touch">
<input id="ajaxcontactkeepintouch" name="ajaxcontactkeepintouch" class="form-control" type="checkbox">
<div><b>Keep in-touch -</b> Please tick here if you do not want to receive information from Tilley Green.<br/>
Please note we will not pass your details onto third parties.</div>
  
</div>    

<a  class="btn send" onclick="ajaxformsendmail(ajaxcontactname.value,ajaxcontactemail.value,ajaxcontactphone.value,ajaxcontactcontents.value, ajaxcontactkeepintouch.value);" style="cursor: pointer">Send</a>

</div>

</form>

<div id="ajax"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i><span class="sr-only">Loading...</span></div>

';
}
add_shortcode('AjaxContactForm', 'createAjaxContactForm');


function ajaxcontact_enqueuescripts() {
    
    wp_enqueue_script('ajaxcontact', get_bloginfo('url').'/wp-content/themes/tilleygreen/js/contact.js', array('jquery'));
    wp_localize_script( 'ajaxcontact', 'ajaxcontactajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

add_action('wp_enqueue_scripts', 'ajaxcontact_enqueuescripts');

function ajaxcontact_send_mail(){
    $results = '';
    $error = 0;
    $name = $_POST['acfname'];
    $email = $_POST['acfemail'];
    $phone = $_POST['acfphone'];
    $contents = $_POST['acfcontents'];
    $keepintouch = $_POST['keepintouch'];
    $admin_email = get_option('admin_email');
    
    if( strlen($email) == 0 ){
        $results = "You must enter an email address.";
        $error = 1;
    }
    elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $results = $email." :email address is not valid.";
        $error = 1;
    }
    elseif( strlen($name) == 0 ){
        $results = "You must enter a name.";
        $error = 1;
    }

    elseif( strlen($contents) == 0 ) {
        $results = "You must enter a message.";
        $error = 1;
    }
    
    if($error == 0){
        $headers = 'From:'.$email. "rn";
        if(wp_mail($admin_email,'Website Message','Name:' . $name . '    Email:' . $email . '    Telephone:' . $phone . 
                                 '    Message:' . $contents . '    Mailing List?:' . $keepintouch, $headers)){
            $results = "Thanks for your message. We'll get back to you as soon as possible. However, if your query is urgent then do please feel free to call.";
       }
        else{
            $results = "*The mail could not be sent.*";
        }
    }

    // Return the String
    die($results);

}

// creating Ajax call for WordPress
add_action( 'wp_ajax_nopriv_ajaxcontact_send_mail', 'ajaxcontact_send_mail' );
add_action( 'wp_ajax_ajaxcontact_send_mail', 'ajaxcontact_send_mail' );


/* ***************************************************** */
/* Woo commerce mods                                     */
/* ***************************************************** */

add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +
 
function woo_custom_cart_button_text() {
 
        return __( 'ADD TO BASKET', 'woocommerce' );
 
}

add_action( 'woocommerce_flat_rate_shipping_add_rate', 'add_another_custom_flat_rate', 10, 2 );

function add_another_custom_flat_rate( $method, $rate ) {
			$new_rate          = $rate;
 			$new_rate['id']    .= ':' . 'rate_per_item'; // Append a custom ID.
 			$new_rate['label'] = 'Rate per item'; // Rename to 'Rushed Shipping'.
 			$new_rate['cost']  += 2; // Add $2 to the cost.

 			// Add it to WC.
 			$method->add_rate( $new_rate );
}

add_filter( 'woocommerce_evaluate_shipping_cost_args','qty_shipping');
        
function coffee_strength($strength){
    echo '<div class="strength"><span class="strength-overlay" style="width:'. $strength * 20 . '%"></span>
	<span class="strength-filler" style="width:'. (5 - $strength) * 20 . '%"></span></div>';
}

add_filter( 'woocommerce_breadcrumb_defaults', 'change_breadcrumb_delimiter' );
function change_breadcrumb_delimiter( $defaults ) {
	// Change the breadcrumb delimeter from '/' to '>'
	$defaults['delimiter'] = ' | ';
        $defaults['home'] = 'Home';
	return $defaults;
}


add_filter( 'woocommerce_product_add_to_cart_text', 'buy_now' );
function buy_now( $args ) {
	

	return 'Buy Now';
}

add_filter( 'woocommerce_grouped_price_html', 'only_show_first_price' );
function only_show_first_price( $args ) {

    $one = substr($args, 0, strpos($args, "</span>")) . "</span>";
    return $one;
  
}



// Our hooked in function - $fields is passed via the filter!
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
     unset($fields['billing']['billing_company']);
     unset($fields['shipping']['shipping_company']);
     return $fields;
} 
        
