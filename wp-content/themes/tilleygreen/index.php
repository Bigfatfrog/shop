<?php get_header(); ?>
<div id="main">
    
     <div class="page-image">

  <?php the_post_thumbnail('slider', array('class' => 'img-responsive')); ?>

     </div>    
    
    
    <div id="content">

     <?php custom_breadcrumbs(); ?>

         <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

         <?php if(!is_page()) {
             echo "<h1>" . get_the_title() . "</h1>Posted on " . get_the_time('F jS, Y') . "</h4>";
         }    ?>

         <p><?php the_content(__('(more...)')); ?></p>
         <hr> <?php endwhile; else: ?>
         <p><?php _e('Sorry, no posts matched your criteria.'); ?></p><?php endif; ?>
    </div>

</div>
<div id="delimiter">
</div>
<?php get_footer(); ?>